﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace LINQ_1
{
    class Program
    {
        static void Main(string[] args)
        {
            User[] Users = {
                                new User(" Марк",        "" ,       "gangs",         "qwerty",          "Марк@gmail.com",           15),
                                new User("ANn",         "Andrews",  "AnnAndr",       "aaadddewww1",     "Ann@gmail.com",            19),
                                new User("Kerry ",      "Crossman", "Valentino",     "ffdsa",           "Valentino12@gmailcom",     52),
                                new User("Adam",        "Brown",    "SaLLla :)",     "ffdsa",           "Adam@gmail.com",           17),
                                new User("Matthew",     "Carter",   "MattCarter",    "123sar",          "Matthew@gmail.com",        21),
                                new User("geoffrey",    "",         "Pixel",         "aaadddewww1",     "Geoffrey@gmaicom",         21),
                                new User("Helen",       "Dyson",    "style_red",     "aaadddewww1",     "Helengmail.com",           29),
                                new User("Caroline",    "",         "borman",        "qwerty",          "Caroline@gmail.com",       15),
                                new User(" Karen ",     "Coleman",  "Bradberry",     "aaadddewww1",     "KarenBradberry@gmail.com", 31),
                                new User("MaRtin",      "Dean",     "Sam-Sam",       "qwerty",          "Martin@gmail.com",         44)
                           };

            Console.WriteLine("Name\tSurname\tLogin\tPassword\tEmail\tAge");
            foreach (var us in Users)
            {
                Console.WriteLine("{0},\t{1},\t{2},\t{3}\t{4}\t{5}\t", us.Name, us.Surname, us.Login, us.Password, us.Email, us.Age);
            }

            Console.WriteLine("Выберите номер запроса:\n");
            Console.WriteLine("1. Количество пользователей, у которых не указано поле Surname\n" +
                              "2. Всех пользователей с логином, начинающимся с буквы “S”;\n" +
                              "3. У всех ли пользователей указан Email;\n" +
                              "4. Имена всех пользователей старше 18 лет (Select);\n" +
                              "5. Все логины, отсортированные по возрастанию;\n" +
                              "6. Количество пользователей, у которых указано только имя или только фамилия;\n" +
                              "7. Есть ли пользователи младше 20 лет;\n" +
                              "8. У всех ли пользователей уникальный логин;\n" +
                              "9. Полные имена всех пользователей;\n" +
                              "10. Логины пользователей с некорректными почтовыми адресами;\n" +
                              "11. Всех пользователей, у которых в логине содержится имя или фамилия;\n" +
                              "12. Количество пользователей, у которых в email содержится логин;\n" +
                              "13. Логины всех пользователей с указанными именем и фамилией, отсортированных по убыванию возраста;\n" +
                              "14. Нормализованные имена всех пользователей (1 буква заглавная, остальные - строчные, пробелы в начале и конце удалены);\n" +
                              "15. Есть ли хоть один пользователь с паролем “qwerty”;\n" +
                              "16. Рейтинг популярности паролей. Вывести список паролей и в скобках - количество пользователей с этим паролем. Отсортировать по количеству;\n" +
                              "17. Создать словарь с логином в качестве ключа и полными именеми пользователей в качестве значений;\n" +
                              "18. Создать словарь с email в качестве ключа и пользователями в качестве значений;\n" +
                              "19. Из массива, содержащего различные логины создать список пользователей;\n" +
                              "20. Создать список логинов (бан-лист). Найти в исходном списке всех пользователей, которые занесены бан-лист;\n" +
                              "21. По словарю, полученному в задании r создать список всех пользователей с логином, состоящим менее, чем из 6 букв.\n");
            int request = Convert.ToInt32(Console.ReadLine());
   
            switch (request)
            {
                case 1: 
                    _1Request(Users); 
                    break;

                case 2:
                    _2Request(Users);
                    break;

                case 3:
                    _3Request(Users);
                    break;

                case 4:
                    _4Request(Users);
                    break;

                case 5:
                    _5Request(Users);
                    break;

                case 6:
                    _6Request(Users);
                    break;

                case 7:
                    _7Request(Users);
                    break;

                case 8:
                    _8Request(Users);
                    break;

                case 9:
                    _9Request(Users);
                    break;

                case 10:
                    _10Request(Users);
                    break;

                case 11:
                    _11Request(Users);
                    break;

                case 12:
                    _12Request(Users);
                    break;

                case 13:
                    _13Request(Users);
                    break;

                case 14:
                    _14Request(Users);
                    break;

                case 15:
                    _15Request(Users);
                    break;

                case 16:
                    _16Request(Users);
                    break;

                case 17:
                    _17Request(Users);
                    break;

                case 18:
                    _18Request(Users);
                    break;

                case 19:
                    _19Request(Users);
                    break;

                case 20:
                    _20Request(Users);
                    break;

                case 21:
                    _21Request(Users);
                    break;
            }

            Console.ReadLine();
        }

        static void _1Request(User[] Users)
        {
            var UserList = from us in Users
                           where us.Surname == null
                           select us;

            Console.WriteLine("Количество пользователей, у которых не указано поле Surname равно " + UserList.Count());
        }

        static void _2Request(User[] Users)
        {
            var UserList = from us in Users
                           where us.Login[0] == 'S'
                           select us;

            foreach (var us in UserList)
            {
                Console.WriteLine("Login: {0}, Name: {1}, Surname: {2}, Password: {3}, Email: {4}, Age: {5}", us.Login, us.Name, us.Surname, us.Password, us.Email, us.Age);
            }
        }

        static void _3Request(User[] Users)
        {
            var UserList = from us in Users
                           where us.Email == null
                           select us;

            if (UserList.Count() == 0)
                Console.WriteLine("Все пользователи заполнили поле Email.");
            else
                Console.WriteLine("Есть пользователи, которые не заполнили поле Email.");
        }

        static void _4Request(User[] Users)
        {
            var UserList = from us in Users
                           where us.Age >= 18
                           select us.Name;

            Console.WriteLine("Имена пользователей, достигших 18 лет");
            foreach (var us in UserList)
                Console.WriteLine(us);
        }

        static void _5Request(User[] Users)
        {
            var UserList = from us in Users
                           orderby us.Login
                           select us.Login;

            Console.WriteLine("Логины пользователей отсортированные по возрастанию");
            foreach (var us in UserList)
                Console.WriteLine(us);
        }

        static void _6Request(User[] Users)
        {
            var UserList = from us in Users
                           where (us.Name != null && us.Surname == null) || (us.Name == null && us.Surname != null)
                           select us;

            Console.WriteLine("Количество пользователей, у которых указано только имя или только фамилия равно " + UserList.Count());
        }

        static void _7Request(User[] Users)
        {
            var UserList = from us in Users
                           where us.Age < 20
                           select us;

            if (UserList.Count() != 0)
                Console.WriteLine("В базе данных есть пользователи младше 20.");
            else
                Console.WriteLine("В базе данных все пользователи старше 20.");
        }

        static void _8Request(User[] Users)
        {
            var UserList = from us1 in Users
                           from us2 in Users
                           where us1.Login == us2.Login
                           select us1.Login;

            if (UserList.Count() != 0)
                Console.WriteLine("Все пользователи имеют уникальные логины");
            else
                Console.WriteLine("Не все пользователи имеют уникальные логины");
        }

        static void _9Request(User[] Users)
        {
            var UserList = from us in Users
                           select us;

            foreach (var us in UserList)
                Console.WriteLine("{0} {1}", us.Name, us.Surname);
        }

        static void _10Request(User[] Users)
        {
            var UserList = from us in Users
                           where us.Email.IndexOf("@") == -1 || us.Email.IndexOf(".") == -1
                           select us.Email;

            foreach (var us in UserList)
                Console.WriteLine(us);
        }

        static void _11Request(User[] Users)
        {
            {
                var UserList = from us in Users
                               where (us.Login.IndexOf(us.Name) != -1 && us.Name != "") || (us.Login.IndexOf(us.Surname) != -1 && us.Surname != "") 
                               select us;

            foreach (var us in UserList)
                Console.WriteLine("Name: {0},\tSurname: {1},\tLogin: {2}", us.Name, us.Surname, us.Login);
            }
         }//норм!

        static void _12Request(User[] Users)
        {
            var UserList = from us in Users
                           where us.Email.IndexOf(us.Login) != -1
                           select us;

            Console.WriteLine("Количество пользователей, у которых Email содержит логин равно {0}", UserList.Count());
        }

        static void _13Request(User[] Users)
        {
            var UserList = from us in Users
                           orderby us.Age descending
                           select us;

            Console.WriteLine("Логины всех пользователей с указанными именем и фамилией, отсортированных по убыванию возраста;");
            foreach(var us in UserList)
                Console.WriteLine("Login: {0}\tName {1}\tSurname {2}", us.Login, us.Name, us.Surname);
        }

        static void _14Request(User[] Users)
        {  
            
            var UserList = from us in Users
                           select ToFormat(us.Name);


            foreach (var us in UserList)
            {
                Console.WriteLine(us);
            }
        }

        static void _15Request(User[] Users)
        {
            var UserList = from us in Users
                           where us.Password.Equals("qwerty")
                           select us;

            Console.WriteLine("Количество пользователей с паролем “qwerty” равно {0}", UserList.Count());
        }

        static void _16Request(User[] Users)
        {
            var UserList = from us in Users
                           group us by us.Password
                               into ws
                               orderby ws.Count() descending
                               select ws;

            foreach (var us in UserList)
            {
                Console.WriteLine("Пароль: {0}, ({1})", us.Key, us.Count());
               
            }
        }

        static void _17Request(User[] Users)
        {
            var myDictionaryFullNameByLogin = (from us in Users
                                               group new { us.Name, us.Surname } by us.Login
                                                   into myDictionary
                                                   select myDictionary).ToDictionary(mdf => mdf.Key, mdf => mdf.ToList());


            foreach (var us in myDictionaryFullNameByLogin)
            {
                Console.Write("Ключ-логин: {0},\t Значение-полное имя: ", us.Key);
                foreach (var val in us.Value)
                {
                    Console.Write(" {0}", val);
                }
                Console.WriteLine();
            }
        }

        static void _18Request(User[] Users)
          {
              var UserList = (from us in Users
                             group new {us.Name, us.Surname, us.Login, us.Password, us.Age} by us.Email).ToDictionary(ul => ul.Key, ul => ul.ToList());
            
              foreach (var us in UserList)
              {
                  Console.Write("Ключ-email: {0}, значение-пользователи:", us.Key);
                
                      foreach(var val in us.Value)
                      {
                          Console.Write(" {0}", val);
                      }
                  Console.WriteLine();
              }
           
          }

        static void _19Request(User[] Users)
        {
            string[] Login = { "Valentino", "Sam-Sam", 
                               "Pixel", "borman", 
                               "Bradberry", "gangs", 
                               "MattCarter", "AnnAndr", 
                               "SaLLla :)", "style_red" };

            var UserList = from us1 in Login
                           join us2 in Users
                           on us1 equals us2.Login
                           into lst
                           select new { Log = us1, TList = lst };

            foreach (var us in UserList)
            {
                Console.WriteLine("К логину {0} относится пользователь со следующими данными:", us.Log);
                foreach (var val in us.TList)
                {
                    Console.WriteLine("Name: {0},\tSurname: {1}", val.Name, val.Surname);
                }
            }
        }

        static void _20Request(User[] Users)
        {
            var BanList = new List<string>();
            BanList.Add("Valentino");
            BanList.Add("borman");     
            BanList.Add("Sam-Sam");  
            BanList.Add("Bradberry");  
            BanList.Add("gangs");  
            BanList.Add("style_red");

            var UserList = from us1 in BanList
                           join us2 in Users
                           on us1 equals us2.Login
                           into lst
                           select new { Ban = us1, BList = lst };

            foreach (var us in UserList)
            {
                Console.WriteLine("Бан-лист: {0}", us.Ban);
                foreach (var val in us.BList)
                    Console.WriteLine("Данные пользователя: Name {0},\tSurname: {1},\tEmail: {2}", val.Name, val.Surname, val.Email);
            }
        }

        static void _21Request(User[] Users)
        {
            var UserList1 = (from us in Users
                             group new { us.Name, us.Surname, us.Login, us.Password, us.Age } by us.Email
                             into g
                             select g).ToDictionary(x => x.Key, x => x.ToList()); // Четвертый день мучаюсь, но я не знаю как не дублировать этот код из _18Request

            foreach (var us in UserList1)
            {
                Console.Write("Ключ-email: {0}, значение-пользователи:", us.Key);

                foreach (var val in us.Value)
                {
                    Console.Write(" {0}", val);
                }
                Console.WriteLine();
            }

            var UserList2 = from us in UserList1
                            select us.Value[0]
                                into x
                                where x.Login.Count() < 6
                                select x;

            foreach (var i in UserList2)
                Console.WriteLine(i);
        }

        static string ToFormat(string Str)
        {
            var myStringBuilder = new StringBuilder();
            foreach (char t in Str)
            {
                myStringBuilder.Append(char.ToLower(t));
            }

            while (myStringBuilder[0] == ' ')
            {
                myStringBuilder.Remove(0,1);
            }

            while (myStringBuilder[myStringBuilder.Length - 1] == ' ')
            {
                myStringBuilder.Remove(myStringBuilder.Length - 1, 1);
            }

            myStringBuilder[0] = char.ToUpper(myStringBuilder[0]);

            return myStringBuilder.ToString();
        }
    }

    class String
    {
        
        public string ToFormat1()
        {
            string str = "as";
            return str;
        }
    }

    class User
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set;}
        public int Age { get; set; }

        public User(string n, string s, string l, string p, string e, int a)
        {
            Name = n;
            Surname = s;
            Login = l;
            Password = p;
            Email = e;
            Age = a;
        }

    }
}
